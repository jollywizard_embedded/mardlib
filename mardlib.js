#!/usr/bin/env node

const fs = require('fs-extra')
const path = require('path')

var commander = require('commander')

var removals = []
var libraries = []
var dest = null

main()

function main()
{
    try 
    {
        processArgs()
              
        if (commander.summary)
           debug_output()
        
        if (commander.dryRun)
            process.exit(0)

        for (let r of removals)
            uninstall(dest, r)
            
        installLibraries(dest, libraries) 
    }
    catch (e) 
    {
        console.error("ERROR:",e)
        process.exit(1)
    }
}

function addRemoval(lib)
{
    removals.push(libraryName(lib))
}

function addLibrary(lib)
{
    libraries.push(path.resolve(lib))
}

function processArgs()
{
    commander
        .version('0.0.1')
        .option('-f, --force', 'Ignore all errors and attempt all operation.')
        .option('-l, --local [value]', 'A local Arduino library folder install.', collect, [])
        .option('-r, --remove [value]', 'A local Arduino library folder to remove.', collect, [])
        .option('-d, --dest [value]', '(Optional) The target location for copy/install.')
        .option('--mode [mode]', '`link` for sym-link (default) or `copy` for copy.', /^(link|copy)/i, 'link')
        .option('--dry-run', 'Describe operation, but do not copy.')
        .option('--no-summary', "Dont write the summary (only operation results).")
        .option('--replace', 'Replace if already installed.')
        .parse(process.argv)

    /* Use default destination if not specified. */
    dest = commander.dest
    if (!dest) dest = getDefaultArduinoLibraryFolder()

    for (let l of commander.remove)
        addRemoval(l)
    
    for (let l of commander.local)
        addLibrary(l)
        
    if (libraries.length == 0 && removals.length == 0)
        addLibrary('.')

    verifyArgs()

    /* Helper */
    function collect(val, memo) {memo.push(val);return memo}

    /* Helper */
    function verifyArgs() 
    {
        verifyTarget(dest)
        for (let l of libraries)
        {
            verifyLibrary(l)
        }
    }
}

function debug_output() 
{
    console.log("Arduino IDE library folder: \n\t", require('path').resolve(dest));

    if (removals.length > 0)
    {
        console.log("Libraries to Remove:")
        for (let l of removals)
            console.log('\t', l, '\t', libraryDest(dest, l))
    }

    if (libraries.length > 0)
    {
        console.log("Libraries to Install:")
        for (let l of libraries)
        {
            console.log("\t %j", l)
            console.log("\t TO %j\t%s", libraryDest(dest, l)
                , libraryInstalled(dest, l) ? "(*exists*)" : "")
        }
    }
        
    if (commander.mode)
        console.log('[ --mode %j ]', commander.mode)
    if (commander.replace)
        console.log('[ --replace ]')
    if (commander.dryRun)
        console.log("[ --dry-run ]")
}

function installLibraries(dest, libraries)
{
    console.log('INSTALLING LIBRARIES')
    
    for (let l of libraries)
    {   
        try 
        {
            verifySafety(dest, l)
        }
        catch (err)
        {
            if (commander.replace)
            {
                console.log("WARNING:", err)
                uninstall(dest, l)
            }
            else
                throw err
        }
        
        if (/^link/i.exec(commander.mode))
            install_by_link(dest, l)
        else
            install_by_copy(dest, l)
    }
}

function uninstall(dest, library)
{
    let target = libraryDest(dest, library)
    fs.removeSync(target)    
    console.log("REMOVED: %j", target)
}

function install_by_copy(dest, library)
{
    let target = libraryDest(dest, library)
    try {
        fs.copySync(library, target)
        console.log('COPIED:\t' + library)
        console.log('    TO:\t' + target)
    } catch (err) {
        throw err
    }
}

function install_by_link(dest, library)
{
    let target = libraryDest(dest, library)
    fs.symlinkSync(library, target)   
    console.log('LINKED: %j TO', library, target);
}

/*
    @return The folder path for the source library if installed at dest.
*/
function libraryDest(dest, library)
{
    return dest + '/' + libraryName(library)
}

function libraryName(library) { 
    return path.basename(library) 
}

/* Verify the installation root is a valid directory. */
function verifyTarget(dest)
{
    try     { fs.statSync(dest).isDirectory() }
    catch(e){ throw "Invalid Library Destination: " + dest }
}

/* Verify the library source is a legitimate library. */
function verifyLibrary(lib)
{
    if (!fs.existsSync(lib) || !fs.statSync(lib).isDirectory())
        throw "Folder does not exist: " + lib
    
    if (!commander.force)
    if (!fs.existsSync(lib + "/library.properties"))
        throw "No `library.properties`: " + lib
}

function libraryInstalled(dest, lib)
{
    return fs.existsSync(libraryDest(dest, lib))
}

/* Check that the library is */
function verifySafety(dest, lib)
{
    if (libraryInstalled(dest, lib))
        throw "Library is already installed: " + dest
}

/*
  Returns the folder path for the location
  that Arduino IDE uses for libraries (by default).
*/
function getDefaultArduinoLibraryFolder()
{   
    const os = require('os')
    var platform = os.platform();
    
    var r = os.userInfo().homedir 
    
    /* Nested in User Documents ? */
    if (/win32|osx/.exec(platform))
        r += "/Documents/"
    
    r += "/Arduino/"
    
    /* Windows nested libraries location */
    if (/win32/.exec(platform))
        r += "/Libraries/"
    
    /* Linux nested libraries */
    if (/linux/.exec(platform))
        r += "/libraries/"
    
    return r;
}

