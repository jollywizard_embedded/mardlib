![Logo][Logo]

# mardlib | My Arduino Libraries

Install Arduino Libraries from source folders.

## Overview

When using C/C++ modules (.h/.c pairs) in separate directories, Arduino IDE only works completely when the referenced modules are located in the global libraries folder and properly packaged and nested.  To reuse modular code between projects, the modules must transition from locally available development files to globally installed resources.

`mardlib` locates the global library folder and links or copies local library projects to it.  You can think of it as `npm link` or `npm install -g`, but for Arduino IDE / libraries.

## Installation

`npm install -g mardlib`

## Usage

~~~
Usage: mardlib [options]

Options:

  -h, --help            output usage information
  -V, --version         output the version number
  -f, --force           Ignore all errors and attempt all operation.
  -l, --local [value]   A local Arduino library folder install.
  -r, --remove [value]  A local Arduino library folder to remove.
  -d, --dest [value]    (Optional) The target location for copy/install.
  --mode [mode]         `link` for sym-link (default) or `copy` for copy.
  --dry-run             Describe operation, but do not copy.
  --no-summary          Dont write the summary (only operation results).
  --replace             Replace if already installed.
~~~

## TODO

* Build a local sketchbook folder and link in all project resources, then boot IDE at that location.
* Also include command syntax: i.e. `mardlib install` and `mardlib link`
* support git repository urls
* have a command to just echo the arduino library.
* replace on force

[Logo]: https://unpkg.com/mardlib@0.0.1/resources/logo_dark.png