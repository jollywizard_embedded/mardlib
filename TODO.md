# mardlib | TODOs

## IDE Helper library

Create Helper library arduino-ide-toofls

  * Locate system command.
  * Invoke verision information
  * Locate preferences file (ask ide and/or search defaults)
  * (Later) Download and install ide (curl/.install)
  * Invoke --version
  * Invoke --get-pref
  * Invoke --set-pref
  * Create arbitrary config file.
  * Install arbitrary config file.
  * Invoke start with arbitrary config file.
  * Build local sketchbook from dependency object map.
  
## Resource locator  

Create a library source reference
```
{
     "local"  :    "Local Source Location"
,    "node"   :    "npm coordinates for published source"
,    "git"    :    "git url for source. (Default try github / bitbucket / <prefix-list>)"
,    "arduino":    "Arduino ecosystem coordinates, i.e. `arduino --install %coord"
}
```

Create helpers for most common tasks.

* Install  (implicit sketchbook)
* Clean    (implicit sketchbook)
* Download (implicit cachefolder)


## Shade

Like maven shade.  

Install to sketchbook libraries with ids obfuscated to prevent conflicts.

Map `library.properties` to a new version that has new name and descriptions that help locate the source materials (repo / local location).

Modules will have to be aliased.  Is this even feasible?  If so, copy will be required instead of link.

Folder name has to be changed if coexist in sketchbook.

This can be expanded to perform an in place patch from a sparse repo.

## extended command syntax

* `-s, --sketchbook [dir]` the target sketchbook directory for library install operations.

* `-n, --node [npmKey]` use the npm id key as a remote source 
    * `--node all` use all node dependencies as arduino libraries
    * `--node auto` use only valid library dependencies as arduino libraries (default)
    * `--node none`

* `--replace` resolve conflicts by removing targets
* `--skip` resolve conflicts by skipping install
* need a mode to replace conflicts, but not configurations, or specify config files (good for Adafruit GFX displays)

* `--save` use `npm --save` on libraries
* `--scan-node` detect and verify the node dependencies
* `-g, --global` install the libraries to the global sketchbook

* `--verify` check whether or not each library is valid before allowing it on the list.

* `--mode config` update config files, but do not link
* `--mode dry` describe operations, but do not modify files or directories.

* `--list-includes` show the list of includes 

### install / link

Move default behaviour here.

### find

Use to find the important arudino related folders

* `mardlib find ide` : folder / command to execute for ide
* `mardlib find libraries` : the subdirectory of sketchbook for libraries (if different)
* `mardlib find sketchbook` : the directory destination for install operations. 

### scan

Use the scanner / library validator to locate important info

* `mardlib scan local` : which local folders/subfolders are arduino libraries (including self)
* `mardlib scan node` : which node modules are arduino libraries
* `mardlib scan installed` : which library modules are installed

Use includes to list which includes are available.

### save command

implies `--save-npm` and `--mode config`

## mardlib.json implementation

Config Keys

* `dependencies` : a list of dependencies.
  * infer type or use subcategory dependency keys.

## dependency keys

* `node` : a list of node ids (npm name or git url)
* `arduino` : a list of arduino package ids to be installed by the ide